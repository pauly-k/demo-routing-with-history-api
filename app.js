console.clear()
console.log('start');

const routes = [
	{
		name: 'home',
		path: '/',
		component: '#home',
	},
	{
		name: 'tasks',
		path: '/tasks',
		component: '#tasks',
	},
	{
		name: 'about',
		path: '/about',
		component: '#about',
	}
]

start()

function start() {
	createNavLinks()
	renderRoute()

	window.onpopstate = evt => {
		renderRoute()
	}	
}


function createNavLinks() {
	const nav = query('nav')

	routes.forEach(route => {
		const link = element('a')
		link.textContent = route.name
		attr(link, 'data-name', route.name)
		attr(link, 'data-path', route.path)
		attr(link, 'data-component', route.component)
		link.onclick = routeLinkClick

		append(nav, link)
	})
}

function routeLinkClick(evt) {
	let path = evt.target.getAttribute('data-path')	

	if (document.location.pathname !== path) {
		history.pushState({}, null, path)
	} else {
		console.log('already on path: ', path);
	}

	renderRoute()
}

function renderRoute() {
	let path = document.location.pathname

	routes.forEach(route => {
		route.show = route.path === path

		const el = query(route.component)
		if (route.show) {
			el.classList.add('show')
		} else {
			el.classList.remove('show')
		}
	})
}






/* ---------------- */
function query(query) {
	return document.querySelector(query)
}


function element(type) {
  return document.createElement(type)
}

function append(target, node) {
  target.appendChild(node)
}

function detach(node) {
  node.parentNode.removeChild(node)
}

function text(data) {
  return document.createTextNode(data)
}

function attr(node, attribute, value) {
  if (value === null) {
    node.removeAttribute(attribute)
  } else if (node.getAttribute(attribute) !== value) {
    node.setAttribute(attribute, value) 
  } 
}

function space() {
  return text(' ')
}

function empty() {
  return text('')
}

function children(element) {
  return Array.from(element.childNodes)
}
