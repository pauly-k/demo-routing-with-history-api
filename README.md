# Demo very basic routing

* display different "components" based on `document.location.pathname`
* react to `window.onpopstate` to render appropriate "route" based on path
* loads appropraite route on initial `onload`

## Not implemented

* route parameters
* route not found
* nested routes
